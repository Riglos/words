// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import Vuex from "vuex";
import axios from "axios";
import VueAxios from "vue-axios";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.use(Vuex);
Vue.use(VueAxios, axios);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

const store = new Vuex.Store({
  state: {
    words: [
      { id: 0, text: "prueba", bold: false, italic: false, underline: false }
    ],
    newWord: {
      id: 1,
      text: "",
      bold: false,
      italic: false,
      underline: false,
      synonyms: [
        { value: null, text: "Please select an option" },
        { value: "a", text: "This is First option" },
        { value: "b", text: "Selected Option" },
        { value: { C: "3PO" }, text: "This is an option with object value" },
        { value: "d", text: "This one is disabled", disabled: true }
      ]
    },
    selected: null,
    synonyms: [
      { value: null, text: "Please select an option" },
      { value: "a", text: "This is First option" },
      { value: "b", text: "Selected Option" },
      { value: { C: "3PO" }, text: "This is an option with object value" },
      { value: "d", text: "This one is disabled", disabled: true }
    ]
  },
  mutations: {
    addNewWord() {
      console.log("addnewword:", this.state.words);
      this.state.words.push(this.state.newWord);
      let index = this.state.words.length;
      console.log("index: ", index);
      this.state.newWord = {
        id: index,
        text: "",
        bold: false,
        italic: false,
        underline: false,
        synonyms: [
          { value: null, text: "Please select an option" },
          { value: "a", text: "This is First option" },
          { value: "b", text: "Selected Option" },
          { value: { C: "3PO" }, text: "This is an option with object value" },
          { value: "d", text: "This one is disabled", disabled: true }
        ]
      };
    },
    deleteWord(word) {
      this.words.$remove(word);
    },
    bold(state, { word }) {
      this.state.words.filter((value, index) => {
        if (value == word) {
          this.state.words[index].bold = !this.state.words[index].bold;
        }
      });
    },
    italic(state, { word }) {
      this.state.words.filter((value, index) => {
        if (value == word) {
          this.state.words[index].italic = !this.state.words[index].italic;
        }
      });
    },
    underline(state, { word }) {
      this.state.words.filter((value, index) => {
        if (value == word) {
          this.state.words[index].underline = !this.state.words[index]
            .underline;
        }
      });
    },
    syn(state, { word }) {
      console.log("word to search", word);

      var config = {
        headers: { "Access-Control-Allow-Origin": "*" }
      };
      axios
        .get(
          "https://api.datamuse.com/words?ml=" + word.text + "&max=5"
          // config
        )
        .then(response => {
          console.log(response);
          var newArray = [];
          response.data.map((value, index) => {
            newArray.push({
              value: value.word,
              text: value.word
            });
          });
          console.log(newArray);
          // this.state.synonyms = newArray;
          this.state.words.filter((value, index) => {
            if (value == word) {
              this.state.words[index].synonyms = newArray;
            }
          });
        })
        .catch(e => {
          console.log(e);
        });
    }
  },
  actions: {
    addNewWord({ commit }) {
      commit("addNewWord");
    },
    bold({ commit }, word) {
      commit("bold", word);
    },
    italic({ commit }, word) {
      commit("italic", word);
    },
    underline({ commit }, word) {
      commit("underline", word);
    },
    syn({ commit }, word) {
      commit("syn", word);
    }
  }
});

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: "#app",
  store,
  components: { App },
  template: "<App/>"
});
