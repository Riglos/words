# Simple Text Editor skeleton

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## Description 
The text editor works in two ways, one is through a popup and the other is in a list, where the word appears next to the options available for editing. The way to select the synonym is only in popup.

## Notes
+ `Vuex` was used to handle the states, `Bootstrap` for the interfaces and `AXIOS` for the API call 
+ I do not use the ControlPanel component.
+ It has a test word that is listed at startup.
